package com.instal.demo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.instal.advertiser.sdk.InstalAnalyticsTracker;
import com.mobileapptracker.MobileAppTracker;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * adb shell am broadcast -a com.android.vending.INSTALL_REFERRER -n com.instal.demo/com.instal.advertiser.sdk.CampaignReceiver --es "referrer" "callback_url=http%3A%2F%2Finstal.com%2Ftrkinst%2F%3Fclkid%3Df5a16245-17d2-4255-8acc-6ed5ddb2a685%26utm_source%3Dinstal" --ez "debug" true
 */
public class MainActivity extends Activity {

    public MobileAppTracker mobileAppTracker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my);

        initMAT();

        callMeteoServer(null);

        if (savedInstanceState == null) {
            InstalAnalyticsTracker.getInstance(this).trackEvent("Open");
        }
    }

    private void initMAT() {
        // Initialize MAT
        MobileAppTracker.init(getApplicationContext(), "7574", "11dedc8e19fb1b56af2ef6ebf6795c34");
        mobileAppTracker = MobileAppTracker.getInstance();
        mobileAppTracker.setReferralSources(this);

        // Only if you have pre-existing users before MAT SDK implementation, identify these users
        // using this code snippet.
        // Otherwise, pre-existing users will be counted as new installs the first time they run your app.
        //boolean isExistingUser = ...
        //if (isExistingUser) {
        //    mobileAppTracker.setExistingUser(true);
        //}

        // Collect Google Play Advertising ID
        new Thread(new Runnable() {
            @Override public void run() {
                // See sample code at http://developer.android.com/google/play-services/id.html
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                    mobileAppTracker.setGoogleAdvertisingId(adInfo.getId(), adInfo.isLimitAdTrackingEnabled());
                } catch (IOException e) {
                    // Unrecoverable error connecting to Google Play services (e.g.,
                    // the old version of the service doesn't support getting AdvertisingId).
                    mobileAppTracker.setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                } catch (GooglePlayServicesNotAvailableException e) {
                    // Google Play services is not available entirely.
                    mobileAppTracker.setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                } catch (GooglePlayServicesRepairableException e) {
                    // Encountered a recoverable error connecting to Google Play services.
                    mobileAppTracker.setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                } catch (NullPointerException e) {
                    // getId() is sometimes null
                    mobileAppTracker.setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                }
            }
        }).start();

        // For collecting Android ID, device ID, and MAC address
        // After August 1st 2014, only Google AID should be collected.
        mobileAppTracker.setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        String deviceId = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        mobileAppTracker.setDeviceId(deviceId);
        try {
            WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            mobileAppTracker.setMacAddress(wm.getConnectionInfo().getMacAddress());
        } catch (NullPointerException e) {
        }
    }

    @Override public void onResume() {
        super.onResume();

        // MAT will not function unless the measureSession call is included
        mobileAppTracker.measureSession();
    }

    public void callMeteoServer(View v) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override protected Boolean doInBackground(Void... params) {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?q=Firenze&units=metric&cnt=2");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s = streamToString(in);
                    JSONObject jsonObject = new JSONObject(s);
                    int id = jsonObject.getJSONArray("list").getJSONObject(1).getJSONArray("weather").getJSONObject(0).getInt("id");
                    return id < 700 || id >= 900;
                } catch (Exception ignored) {
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
                return null;
            }

            @Override protected void onPostExecute(Boolean b) {
                findViewById(R.id.progress).setVisibility(View.GONE);
                if (b == null) {
                    View reloadButton = findViewById(R.id.reload);
                    reloadButton.setVisibility(View.VISIBLE);
                } else {
                    TextView resultText = (TextView) findViewById(R.id.result);
                    resultText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Anton.ttf"));
                    resultText.setText(b ? "SI" : "NO");
                    findViewById(R.id.root).setBackgroundColor(b ? 0xFF66b3f2 : 0xFFf96908);
                    resultText.setVisibility(View.VISIBLE);
                }
            }
        }.execute();
    }

    private String streamToString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "utf-8"), 8);

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();

        return sb.toString();
    }
}
